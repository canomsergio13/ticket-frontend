import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Ticket from './pages/Ticket'
import Navbar from './components/NavBar'
import Survey from './pages/Survey'
import ErrorPage from './pages/Error'
import Charts from './pages/Charts'

function App() {

    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Navbar />}>
                    <Route path="" element={<Ticket />} />
                </Route>
                <Route path="/" element={<Navbar />}>
                    <Route path="survey/:encrypted_ticket_id" element={<Survey />} />
                </Route>
                <Route path="/" element={<Navbar />}>
                    <Route path="charts" element={<Charts />} />
                </Route>
                <Route path="/" element={<Navbar />}>
                    <Route path="error" element={<ErrorPage />} />
                </Route>
            </Routes>
        </BrowserRouter>
    )
}

export default App
