import _ from 'lodash'

export const getTypeUser = (names,namesData,type) => {
    const result = _.filter(namesData,data => names.includes(data?.realname) || names.includes(data?.firstname))
        .map(data => ({
            users_id: data.id,
            name: data.name,
            alternative_email: data.email,
            type,
        })
    );

    return result;
} 

