export const HttpCode = Object.freeze({
    STATUS_OK : 200,
    CREATED   : 201,
})