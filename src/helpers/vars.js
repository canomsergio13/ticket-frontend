export const __SERVER__ = import.meta.env.VITE_SERVER_URL
export const UPLOAD_PRESET = import.meta.env.VITE_UPLOAD_PRESET
export const __SERVER_IMAGES__ = import.meta.env.VITE_SERVER_CLOUDINARY
