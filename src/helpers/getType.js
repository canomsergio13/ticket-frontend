export const getType = title => {
    const types =  {
        'SOLICITUD DE IMPRESORAS': 2,
        'SOLICITUD DE NUEVO SERVICIO': 2,
        'PREPARACIÓN DE NUEVO EQUIPO': 2,
        'ACCESO A CARPETAS COMPARTIDAS':2,
        'SERVICIO ESPECIAL PARA PERSONAL V.I.P.': 2,
    }

    return types[title] || 1;
}