export const optionsNames = [
    {
        value: 'DESBLOQUEO DE JD EDWARDS',
        label: 'DESBLOQUEO DE JD EDWARDS',
    },{
        value: 'DESINSTALACIÓN DE KARSPERSKY',
        label: 'DESINSTALACIÓN DE KARSPERSKY',
    },{
        value: 'INSTALACIÓN DE SENTINELONE',
        label: 'INSTALACIÓN DE SENTINELONE',
    },{
        value: 'PROBLEMA CON AUDIO DEL EQUIPO DE CÓMPUTO',
        label: 'PROBLEMA CON AUDIO DEL EQUIPO DE CÓMPUTO',
    },{
        value: 'ACTUALIZACIONES DEL SISTEMA OPERATIVO ',
        label: 'ACTUALIZACIONES DEL SISTEMA OPERATIVO ',
    },{
        value: 'PANTALLA AZUL WINDOWS ',
        label: 'PANTALLA AZUL WINDOWS ',
    },{
        value: 'APLICACIÓN DE GOOGLE (HOJA DE CÁLCULO, CORREO, DOCUMENTO)',
        label: 'APLICACIÓN DE GOOGLE (HOJA DE CÁLCULO, CORREO, DOCUMENTO)',
    },{
        value: 'RECUPERAR UN CORREO',
        label: 'RECUPERAR UN CORREO',
    },{
        value: 'ACCESO A CARPETAS COMPARTIDAS',
        label: 'ACCESO A CARPETAS COMPARTIDAS',
    },{
        value: 'IMPRESORAS',
        label: 'IMPRESORAS',
    },{
        value: 'PREPARACIÓN DE NUEVO EQUIPO',
        label: 'PREPARACIÓN DE NUEVO EQUIPO',
    },{
        value: 'VIDEOCONFERENCIA EN SALA DE JUNTAS',
        label: 'VIDEOCONFERENCIA EN SALA DE JUNTAS',
    },{
        value: 'SERVICIO ESPECIAL PARA PERSONAL V.I.P.',
        label: 'SERVICIO ESPECIAL PARA PERSONAL V.I.P.',
    },{
        value: 'CHECKLIST INFRAESTRUCTURA Y APLICACIONES DRAGON',
        label: 'CHECKLIST INFRAESTRUCTURA Y APLICACIONES DRAGON',
    },{
        value: 'PROBLEMAS CON LA CONEXIÓN POR VPN',
        label: 'PROBLEMAS CON LA CONEXIÓN POR VPN',
    },{
        value: 'PROBLEMAS CON LAS PÁGINAS O PORTALES DE EMPRESAS DRAGON',
        label: 'PROBLEMAS CON LAS PÁGINAS O PORTALES DE EMPRESAS DRAGON',
    },{
        value: 'ERROR EN UN PROCESO JDE- COMPRAS',
        label: 'ERROR EN UN PROCESO JDE- COMPRAS',
    },{
        value: 'ERROR EN UN PROCESO JDE-FACTURACIÓN',
        label: 'ERROR EN UN PROCESO JDE-FACTURACIÓN',
    },{
        value: 'ERROR EN UN PROCESO JDE- ALMACEN',
        label: 'ERROR EN UN PROCESO JDE- ALMACEN',
    },{
        value: 'ERROR EN UN PROCESO JDE-VENTAS',
        label: 'ERROR EN UN PROCESO JDE-VENTAS',
    },{
        value: 'ERROR EN UN PROCESO JDE-FISCAL',
        label: 'ERROR EN UN PROCESO JDE-FISCAL',
    },{
        value: 'ERROR EN UN PROCESO JDE-CONTABILIDAD Y FINANZAS',
        label: 'ERROR EN UN PROCESO JDE-CONTABILIDAD Y FINANZAS',
    },{
        value: 'PROBLEMA CON OSMOS',
        label: 'PROBLEMA CON OSMOS',
    },{
        value: 'PROBLEMA CON QLINK VIEW',
        label: 'PROBLEMA CON QLINK VIEW',
    },{
        value: 'PROBLEMA CON PSICUM',
        label: 'PROBLEMA CON PSICUM',
    },{
        value: 'PROBLEMA CON ENDTOEND',
        label: 'PROBLEMA CON ENDTOEND',
    },{
        value: 'PROBLEMA CON RELOJ CHECADOR',
        label: 'PROBLEMA CON RELOJ CHECADOR',
    },{
        value: 'TELÉFONO - CELULAR O EXTENSIÓN',
        label: 'TELÉFONO - CELULAR O EXTENSIÓN',
    },{
        value: 'RESPALDO DE INFORMACIÓN',
        label: 'RESPALDO DE INFORMACIÓN',
    },{
        value: 'SOLICITUD DE NUEVO SERVICIO',
        label: 'SOLICITUD DE NUEVO SERVICIO',
    },{
        value: 'REPORTES DE GASTO',
        label: 'REPORTES DE GASTO',
    }
]