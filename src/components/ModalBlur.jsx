import { Modal, ModalBody, ModalContent, ModalHeader, ModalOverlay } from "@chakra-ui/react"
import PropTypes from 'prop-types'

const ModalBlur = ({header,body}) => {
    return (
        <Modal isCentered isOpen={true}  >
            <ModalOverlay
                bg='blackAlpha.300'
                backdropFilter='blur(10px) hue-rotate(90deg)'
            />
            <ModalContent className="mx-2">
                {header && <ModalHeader className='text-center pb-1' >{header}</ModalHeader>}
                <ModalBody>
                    { body }
                </ModalBody>
            </ModalContent>
        </Modal>
    )
}

ModalBlur.propTypes = {
    header: PropTypes.string,
    body: PropTypes.node,
}

export default ModalBlur