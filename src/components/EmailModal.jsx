import { Button, useToast } from "@chakra-ui/react"
import { MdSaveAs } from "react-icons/md"
import PropTypes from 'prop-types'
import { verifyEmail } from "../api/email/email"
import { HttpCode } from "../helpers/HttpCode"
import ModalBlur from "./ModalBlur"

const EmailModal = ({setModal,Email, setEmail}) => {
    const toast = useToast()

    const validateEmail = async () => {
        if(Email === ''){
            toast({
                title: 'Falta información.',
                description: "El correo en requerido.",
                status: 'warning',
                duration: 9000,
                isClosable: true,
                position: "top-right"
            })
        } else {
            const res = await verifyEmail({ Email })
            if(res.status === HttpCode.STATUS_OK){
                setEmail(res.data.user[0])
                setModal(false)
            } else {
                toast({
                    title: 'Correo inválido',
                    description: res.data.mess,
                    status: 'warning',
                    duration: 9000,
                    isClosable: true,
                    position: "top-right"
                })
            }
        }
    }

    const handleKeyPress = event => {
        if (event.key === 'Enter') {
            validateEmail()
        }
      };

    return (
        <ModalBlur
            header={'Ingresa tu correo para identificarte'}
            body={
                <form onSubmit={e => e.preventDefault()}>
                    <input 
                        placeholder="Ingresa tu correo"
                        name="email" 
                        type="email" 
                        className="input_email_field text-center" 
                        onChange={e=>setEmail(e.target.value)}
                        onKeyDown={ handleKeyPress }
                        required
                    />

                    <Button
                        className='my-3 w-100'
                        leftIcon={<MdSaveAs />}
                        size="md"
                        bg={'green.400'}
                        color={'white'}
                        _hover={{ bg: 'green.500'}}
                        onClick={validateEmail}
                    >
                        Validar correo
                    </Button>
                </form>
            }
        />
    )
}

EmailModal.propTypes = {
    setModal: PropTypes.func,
    setEmail: PropTypes.func,
    Email: PropTypes.any,
}

export default EmailModal