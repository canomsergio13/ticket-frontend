import { Link, Outlet } from 'react-router-dom';

const Navbar = () =>
    <section>
        <nav className="navbar navbar-expand-lg navbar-dark bg-success px-5">
            <div className="container-fluid">
                <Link to="/" className="navbar-brand d-flex align-items-center">
                    <img src='/dragon-favicon.png' className='mx-1' alt="logo" />
                    Dragón
                </Link>
                <button type="button" className="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse4">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarCollapse4">
                    <ul className="navbar-nav ms-auto">
                        <li className="nav-item">
                            <a
                                target='_blank'
                                rel='noreferrer'
                                href="https://glpi.victum-re.online/"
                                className="nav-link"
                            >Mesa de ayuda</a>
                        </li>
                        <li className="nav-item">
                            <a
                                rel='noreferrer'
                                href="/Charts"
                                className="nav-link"
                            >Estadísticas</a>
                        </li>
                        <li className="nav-item">
                            <a
                                rel='noreferrer'
                                href="/"
                                className="nav-link"
                            >Ticket rápido</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main className='container'>
            <Outlet />
        </main>
    </section>


export default Navbar