import { Button, Result } from 'antd';
import React from 'react';

function ErrorPage() {
    return (
        <Result
            status="404"
            title="¡Oops!"
            subTitle="Lo sentimos, el elemento que estás buscando no está disponible."
            extra={<Button type="primary" onClick={() => window.location.href = '/'}>Ir a la página principal</Button>}
        />
    );
}

export default ErrorPage;
