import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import '../styles/Survey.css'
import { useParams, useNavigate } from 'react-router-dom'
import CryptoJS from 'crypto-js'

import header_img from '../assets/soporte-tecnico.jpeg';
import { storeSurvey } from '../api/survey/survey'
import { Result, Button } from 'antd'
import { useToast } from '@chakra-ui/react'
import { showTicket } from '../api/tickets/tickets'

const RadioOption = ({ id, name, value, handleChange, respuesta }) => {
    return (
        <>
            <input
                type="radio"
                id={id}
                name={name}
                value={value}
                checked={respuesta === value}
                onChange={handleChange}
            />
            <label className="radio-label" htmlFor={id}>{value}</label><br />
        </>
    )
}

const Survey = () => {
    const { encrypted_ticket_id } = useParams();
    const navigate = useNavigate();
    const toast = useToast();

    const QUESTION1 = 'pregunta1';
    const QUESTION2 = 'pregunta2';

    const [ticketId, setTicketId] = useState(null);
    const [ticket, setTicket] = useState({});
    const [loading, setLoading] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    // Definimos el estado para almacenar las respuestas seleccionadas
    const [respuestas, setRespuestas] = useState({
        pregunta1: '',
        pregunta2: '',
        comments: null
    });

    useEffect(() => {
        // Decodificar el parámetro cifrado antes de descifrarlo
        const cifradoDecodificado = decodeURIComponent(encrypted_ticket_id);

        // Función para descifrar el parámetro
        const decryptParam = (cifrado) => {
            const bytes = CryptoJS.AES.decrypt(cifrado, 'Jir@f@2024');
            return bytes.toString(CryptoJS.enc.Utf8);
        }
        // Obtener detalles del ticket
        const getTicketDetails = async (ticket_id) => {
            const fetch = await showTicket({ ticket_id });
            setTicket(fetch);
        }
        // Desciframos el parámetro recibido
        const decryptedTicketId = decryptParam(cifradoDecodificado);

        // Verificar si el ticket_id es válido (podrías hacer una solicitud al servidor para verificar)
        // Aquí simplemente comprobamos si es un número
        if (decryptedTicketId) {
            setTicketId(decryptedTicketId);
            getTicketDetails(decryptedTicketId);
        } else {
            // Redirigir a la página de error si el identificador del ticket no es válido
            navigate('/error')
        }
    }, [encrypted_ticket_id, navigate]);
    // Función para manejar los cambios en las respuestas
    const handleChange = (e) => {
        const { name, value } = e.target;
        setRespuestas(prevRespuestas => ({
            ...prevRespuestas,
            [name]: value
        }));
    };

    // Función para manejar el envío del formulario
    const handleSubmit = async (e) => {
        e.preventDefault();
        if (validateData()) {
            setLoading(true);
            await storeSurvey({ responses: respuestas, ticket_id: ticketId });
            setLoading(false);
            setIsSuccess(true);
        } else {
            toast({
                title: 'Falta información.',
                description: "Asegurese de elegir una calificación",
                status: 'warning',
                duration: 3000,
                isClosable: true,
                position: "top-right"
            })
        }
    };

    const validateData = () => {
        return respuestas.pregunta1 && respuestas.pregunta2
    }

    if (ticketId === null) return null;
    return (
        <div>
            {
                isSuccess ? <Result
                    status="success"
                    title="¡Gracias por completar la encuesta!"
                    subTitle="Su retroalimentación es muy valiosa para nosotros y nos ayudará a mejorar nuestros servicios. Si tiene alguna otra pregunta o inquietud, no dude en ponerse en contacto con nosotros. ¡Gracias por su colaboración!"
                    extra={
                        <Button type="primary" onClick={() => window.location.href = '/'}>Ir a la página principal</Button>
                    }
                /> : <>
                    <h1 className='title-survey'>ENCUESTA DE SATISFACCIÓN - TICKET #{ticketId}</h1>
                    <h2 className='subtitle-survey'>Asunto: {ticket?.name}</h2>
                    <h2 className='subtitle-survey'>Ingeniero: {ticket?.full_name}</h2>
                    <img src={header_img} alt="Encabezado" className='header-img' />
                    <form onSubmit={handleSubmit}>
                        <div className="radio-options">
                            <p className='question'>Tomando en cuenta el tipo de problema, tiempo, forma y atención brindada ¿qué calificación le daría al ingeniero que le atendió?</p>
                            <p className='hint'>Por favor, califique en una escala del 1 al 5, donde 1 representa una experiencia pésima y 5 representa una experiencia excelente.</p>
                            <div className="radio-option">
                                <RadioOption id={QUESTION1 + '1'} name={QUESTION1} value='1' handleChange={handleChange} respuesta={respuestas.pregunta1} />
                                <RadioOption id={QUESTION1 + '2'} name={QUESTION1} value='2' handleChange={handleChange} respuesta={respuestas.pregunta1} />
                                <RadioOption id={QUESTION1 + '3'} name={QUESTION1} value='3' handleChange={handleChange} respuesta={respuestas.pregunta1} />
                                <RadioOption id={QUESTION1 + '4'} name={QUESTION1} value='4' handleChange={handleChange} respuesta={respuestas.pregunta1} />
                                <RadioOption id={QUESTION1 + '5'} name={QUESTION1} value='5' handleChange={handleChange} respuesta={respuestas.pregunta1} />
                            </div>
                        </div>
                        <div className="radio-options">
                            <p className='question'>Tomando en cuenta el tipo de problema, tiempo, forma y atención brindada ¿cuál es su nivel de satisfacción con el servicio de Soporte en Mesa de Ayuda en Empresas Dragón?</p>
                            <p className='hint'>Por favor, califique en una escala del 1 al 5, donde 1 representa una experiencia pésima y 5 representa una experiencia excelente.</p>
                            <div className="radio-option">
                                <RadioOption id={QUESTION2 + '1'} name={QUESTION2} value='1' handleChange={handleChange} respuesta={respuestas.pregunta2} />
                                <RadioOption id={QUESTION2 + '2'} name={QUESTION2} value='2' handleChange={handleChange} respuesta={respuestas.pregunta2} />
                                <RadioOption id={QUESTION2 + '3'} name={QUESTION2} value='3' handleChange={handleChange} respuesta={respuestas.pregunta2} />
                                <RadioOption id={QUESTION2 + '4'} name={QUESTION2} value='4' handleChange={handleChange} respuesta={respuestas.pregunta2} />
                                <RadioOption id={QUESTION2 + '5'} name={QUESTION2} value='5' handleChange={handleChange} respuesta={respuestas.pregunta2} />
                            </div>
                        </div>
                        <div className="radio-options">
                            <p className='question'>Comentarios adicionales</p>
                            <textarea
                                name="comments"
                                cols="30"
                                rows="5"
                                className="comments-textarea"
                                placeholder="Por favor, déjenos sus comentarios adicionales..."
                                value={respuestas.comments}
                                onChange={handleChange}
                                maxLength={100}
                            />
                        </div>
                        <button type="submit" id='button-send' disabled={loading}>{loading ? 'Enviando...' : 'Enviar respuestas'}</button>
                    </form>
                </>
            }
        </div>
    )
}

Survey.propTypes = {}

RadioOption.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    handleChange: PropTypes.func.isRequired,
    respuesta: PropTypes.string.isRequired,

}
export default Survey