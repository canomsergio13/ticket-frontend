import moment from 'moment';
import { useState } from 'react';
import { Button, Text, useToast } from '@chakra-ui/react';
import EmailModal from '../components/EmailModal';
import FirstFormTicket from '../components/FirstFormTicket';
import { createTicket, uploadImage, uploadInfo, usersTickets } from '../api/tickets/tickets';
import ModalBlur from '../components/ModalBlur';
import { Result } from 'antd';
import { UPLOAD_PRESET } from '../helpers/vars';
import Spinner from '../components/Spinner';

const currentHour = () => moment().format('YYYY-MM-DD HH:mm:ss');


const Ticket = () => {
    const toast = useToast()
    const [current] = useState(0);
    const [ModalC, setModal] = useState(true);
    const [NewTicketID, setNewTicketID] = useState(null);

    const [selectedFile, setSelectedFile] = useState(null);
    const [Name, setName] = useState(null);
    const [Email, setEmail] = useState('')
    const [IsLoading, setIsLoading] = useState(false)

    const [Values, setValues] = useState({
        title: "", //user
        description: "", //user
        priority: 3,
        urgency: 3,
        impact: 3,
        origin: 1,
        type: 1, //default
        date: null //default
    })

    const handleFileChange = e => {
        setName(e.target?.files[0].name)
        setSelectedFile(e.target?.files[0])
    }

    const checkImage = id => {
        if (!selectedFile) return false;

        const dataImg = new FormData();
        dataImg.append('file', selectedFile);
        dataImg.append('ticket_id', id);
        dataImg.append('upload_preset', UPLOAD_PRESET);

        return dataImg;
    }

    const create = async () => {
        setIsLoading(true)

        const tickets_user = [
            { users_id: Email.id, type: 1, alternative_email: Email.name }, //solicitante
            { users_id: 10, type: 3, alternative_email: 'jmbravo@consultores-cti.com.mx' }, //observador
        ]

        const ticketData = {
            ...Values,
            date: currentHour(),
            users_id_lastupdater: Email.id
        };

        if (ticketData.otherTitle) {
            ticketData.title = ticketData.otherTitle;
            delete ticketData.otherTitle;
        }
        
        const res = await createTicket(ticketData);
        await usersTickets({ ticket_id: res?.id, tickets_user })

        const formData = checkImage(res?.id)
        if (formData) {
            const resImages = await uploadImage(formData);

            const info = {
                ...resImages,
                created_at: moment(resImages.created_at).format('YYYY-MM-DD HH:mm:ss'),
                ticket_id: res.id,
                users_id: Email.id
            }
            await uploadInfo(info);
        }

        setIsLoading(false)
        if (res?.id) {
            setNewTicketID(res.id)
        } else {
            toast({
                title: 'Ticket NO creado.',
                description: "Hubo un error, intente nuevamente.",
                status: 'error',
                duration: 9000,
                isClosable: true,
            })
        }
    }

    const propsForm = {
        Name,
        Values,
        create,
        setValues,
        handleFileChange,
    }

    const steps = [
        {
            title: 'Datos generales',
            content: <FirstFormTicket  {...propsForm} />,
        }
    ];

    const handleNewTicket = () => {
        setValues({
            ...Values,
            description: "",
            email: "",
            otherTitle: ""
        })
        setNewTicketID(null)
        setSelectedFile(null)
        setName(null)
    }

    const openGLPI = () => window.open('https://glpi.victum-re.online/', '_blank');

    return (
        <section className='px-1 pt-3'>
            {
                ModalC ? (
                    <EmailModal setModal={setModal} setEmail={setEmail} Email={Email} />
                ) : (
                    NewTicketID ? (
                        <ModalBlur
                            body={
                                <Result
                                    status="success"
                                    title={`ID del ticket creado: ${NewTicketID}`}
                                    subTitle={
                                        <Text as='b' fontSize='1.2em'>
                                            El ticket fue creado correctamente
                                        </Text>
                                    }
                                    extra={[
                                        <Button
                                            key='succes'
                                            className='mb-2'
                                            colorScheme='green'
                                            onClick={openGLPI}
                                        >Dar seguimiento</Button>,
                                        <Button
                                            key="new"
                                            className='mb-2'
                                            colorScheme='green'
                                            variant='outline'
                                            onClick={handleNewTicket}
                                        >Crear nuevo ticket</Button>
                                    ]}
                                />
                            }
                        />
                    ) : (

                        IsLoading ? (
                            <ModalBlur
                                body={<Spinner />}
                            />
                        ) : (
                            <section>
                                {steps[current].content}
                            </section>
                        )
                    )

                )
            }
        </section>
    );

};

export default Ticket;