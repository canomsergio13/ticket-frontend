import React from 'react'

const Charts = () => {
    return (
        <iframe
            src="https://metabase-sga.victum-re.online/public/dashboard/181235fa-d152-423f-ad94-7236e1f57b9d"
            frameborder="0"
            style={{ width: '100%', height: '80vh' }}
            allowtransparency
        ></iframe>
    )
}

export default Charts