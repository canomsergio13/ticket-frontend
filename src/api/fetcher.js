import { __SERVER__ } from "../helpers/vars";
import axios from "axios";

const axiosAPIInstance = axios.create({
    baseURL: `${__SERVER__}`,
});

const fetcher = async args => {
    try {
        const response = await axiosAPIInstance(args);
        return response;
    } catch (error) {
        const { response } = error;
        return response || error.request || error.message;
    }
};

export default fetcher;
