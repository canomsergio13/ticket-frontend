import { HttpCode } from "../../helpers/HttpCode";
import fetcher from "../fetcher";

export const getUsers = async () => {

    const url = `/users/all`;
    let response = [];
    try {
        let fetch = await fetcher({
            url,
            method: 'GET'
        });

        (fetch.status === HttpCode.STATUS_OK) && (response = fetch.data)
        
        return response;
    } catch (error) {
        return response;
    }
}