import fetcher from "../fetcher";

export const storeSurvey = async data => {

    const url = `/survey`;
    let response = [];

    try {
        let fetch = await fetcher({
            url,
            method: 'POST',
            data
        });
        // console.log("🚀 ~ file: survey.js:17 ~ storeSurvey ~ fetch:", fetch)
        response = fetch;
        return response;
    } catch (error) {
        return response;
    }
}