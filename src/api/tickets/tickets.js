import { __SERVER_IMAGES__ } from "../../helpers/vars";
import { HttpCode } from "../../helpers/HttpCode";
import fetcher from "../fetcher";

export const createTicket = async ticket => {

    const url = `/ticket`;
    let response = [];

    try {
        let fetch = await fetcher({
            url,
            method: 'POST',
            data: ticket
        });

        (fetch.status === HttpCode.CREATED) && (response = fetch.data)

        return response;
    } catch (error) {
        return response;
    }
}

export const usersTickets = async data => {
    const url = `/ticket_users`;
    let response = [];

    try {
        let fetch = await fetcher({
            url,
            method: 'POST',
            data
        });

        (fetch.status === HttpCode.CREATED) && (response = fetch.data)

        return response;
    } catch (error) {
        return response;
    }
}

export const uploadImage = async data => {
    const url = __SERVER_IMAGES__;

    try {
        const response = await fetch(url, {
            method: 'POST',
            body: data
        });

        const datares = await response.json();
        return datares;
    } catch (error) {
        return {
            mess: `Error uploading image: ${error}`
        }
    }
}

export const uploadInfo = async info => {
    const url = `/upload_info`;
    let response = [];

    try {
        let fetch = await fetcher({
            url,
            method: 'POST',
            data: info
        });

        response = fetch;

        return response;
    } catch (error) {
        return response;
    }
}

export const showTicket = async ({ ticket_id }) => {
    try {
        let response = {}
        const fetch = await fetcher({
            url: `/ticket/${ticket_id}`,
            method: 'GET'
        });
        // console.log("🚀 ~ file: tickets.js:89 ~ showTicket ~ fetch:", fetch)
        if (fetch.status === 200) {
            response = fetch.data[0]
        }
        return response
    } catch (error) {
        return response;
    }
}