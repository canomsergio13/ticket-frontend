
import fetcher from "../fetcher";

export const verifyEmail = async email => {

    const url = `/verify_email`;
    let response = [];

    try {
        let fetch = await fetcher({
            url,
            method: 'POST',
            data: email
        });


        response = fetch;
        
        return response;
    } catch (error) {
        return response;
    }
}